SELECT
  DATE(b.data) data,
  DATE(DATE_ADD(b.data,b.hora,'hour')) prev,
  b.lat,
  b.lon,
  SUM(b.PWAT) PWAT_SUM,
  SUM(b.PRATE*21600) PRATE_SUM,
  SUM(b.APCP) APCP_SUM,
  AVG(b.TMP) TMP_AVG,
  b.raw,
  b.repet,
FROM (
    --Remove dados repetidos--
  SELECT
    a.file_source file_source,
    TIMESTAMP(CONCAT(SUBSTR(a.file_source, 5, 4),'-',SUBSTR(a.file_source, 9, 2),'-',SUBSTR(a.file_source, 11, 2),' 00:00:00')) data,
    INTEGER(SUBSTR(a.file_source, 16, 3)) hora,
    INTEGER(INTEGER(SUBSTR(a.file_source, 16, 3))%6) hora_6h,
    a.Lat lat,
    (a.Lon-360) lon,
    AVG(a.PWAT) PWAT,
    AVG(a.PRATE) PRATE,
    AVG(a.APCP) APCP,
    AVG(a.TMP) TMP,
    a.raw raw,
    COUNT(1) repet
  FROM (
    SELECT
      FIRST(SPLIT(raw, ';')) file_source,
      FLOAT(NTH(2, SPLIT(raw, ';'))) Lat,
      FLOAT(NTH(3, SPLIT(raw, ';'))) Lon,
      FLOAT(NTH(4, SPLIT(raw, ';'))) PWAT,
      FLOAT(NTH(5, SPLIT(raw, ';'))) PRATE,
      FLOAT(NTH(6, SPLIT(raw, ';'))) APCP,
      FLOAT(NTH(7, SPLIT(raw, ';'))) TMP,
      raw,
    FROM
      [pluvion-tech:pluvion_lab.fc_nooa_16d_0h_gfs] )AS a
  WHERE
    a.APCP IS NOT NULL
  GROUP BY
    file_source,
    data,
    hora,
    hora_6h,
    lat,
    lon,
    raw)AS b
WHERE
  b.hora_6h = 0
  AND DATEDIFF((DATE_ADD(b.data,b.hora,'hour')),b.data) IN (0)
GROUP BY
  1,
  2,
  3,
  4,
  9,
  10