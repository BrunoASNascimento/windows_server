SELECT
  *
FROM (
  SELECT
    FIRST(SPLIT(f0, ' ')) ESTACAO,
    FLOAT(NTH(2, SPLIT(f0, ' '))) LATITUDE,
    FLOAT(NTH(3, SPLIT(f0, ' '))) LONGITUDE,
    FLOAT(NTH(4, SPLIT(f0, ' '))) ALTITUDE,
    NTH(5, SPLIT(f0, ' ')) ANO,
    NTH(6, SPLIT(f0, ' ')) MES,
    NTH(7, SPLIT(f0, ' ')) DIA,
    NTH(8, SPLIT(f0, ' ')) HORA,
    FLOAT(NTH(9, SPLIT(f0, ' '))) TEMP,
    FLOAT(NTH(10, SPLIT(f0, ' '))) TMAX,
    FLOAT(NTH(11, SPLIT(f0, ' '))) TMIN,
    FLOAT(NTH(12, SPLIT(f0, ' '))) UR,
    FLOAT(NTH(13, SPLIT(f0, ' '))) URMAX,
    FLOAT(NTH(14, SPLIT(f0, ' '))) URMIN,
    FLOAT(NTH(15, SPLIT(f0, ' '))) TD,
    FLOAT(NTH(16, SPLIT(f0, ' '))) TDMAX,
    FLOAT(NTH(17, SPLIT(f0, ' '))) TDMIN,
    FLOAT(NTH(18, SPLIT(f0, ' '))) PRESSAONNM,
    FLOAT(NTH(19, SPLIT(f0, ' '))) PRESSAONNM_MAX,
    FLOAT(NTH(20, SPLIT(f0, ' '))) VELVENTO,
    FLOAT(NTH(21, SPLIT(f0, ' '))) DIR_VENTO_RAJADA,
    FLOAT(NTH(22, SPLIT(f0, ' '))) RADIACAO,
    FLOAT(NTH(23, SPLIT(f0, ' '))) PRECIPITACAO, 
  FROM
    [pluvion-tech:pluvion_lab.inmet_serv])
WHERE
  LATITUDE IS NOT NULL