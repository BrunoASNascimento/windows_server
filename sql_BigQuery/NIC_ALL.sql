SELECT
  name,
  sigfoxId,
  measuredAtISO,
  data_sao_paulo,
  data_sao_paulo data_hora,
  time(data_sao_paulo ) hora,
  (3.6*wsMax1) as wsMax1,
  (3.6*wsVAvg1)as wsVAvg1,
  (3.6*wsL5m1)as wsL5m1,
  wdVAvg1,
  wdsMax1,
  atpMin1,
  atpMax1,
  atpAvg1,
  atpVar1,
  pcV1,
  arhMin1,
  arhMax1,
  arhAvg1,
  arhVar1,
  latitude,
  longitude
FROM (
  SELECT
    sigfoxId AS id,
    measuredAtISO,
    DATETIME(TIMESTAMP( measuredAtISO ),"America/Sao_Paulo") AS data_sao_paulo,
    wsMax1,
    wsVAvg1,
    wsL5m1,
    wdVAvg1,
    wdsMax1,
    atpMin1,
    atpMax1,
    atpAvg1,
    atpVar1,
    pcV1,
    pcI1,
    arh1,
    arhMin1,
    arhMax1,
    arhAvg1,
    arhVar1,
    msgCode
  FROM
    `pluvion-tech.pluvion_dev.wtr_parsed`
  WHERE
	sigfoxId IN ('41C514','41D2C7','41C533','41C523','41C48F','41C47E','41C488','41C48B','41BF05','41C496','41C516','41C484','41C517','41C493','41C539','41C48A','41C505','41C51A','41C47D','41C527','41CD05','41C59F','41C486','41D2B0')
	and msgCode in ('ws_wd_ard','pc','at_arh') 
  )
LEFT JOIN
  `pluvion-tech.pluvion_lab.NIC_ESTACOES`
ON
  id=sigfoxId
ORDER BY
  4,
  1