SELECT
  forecast_type,
  if (id ='USM_5',
    'USM - Ibitirama',
    if (id ='USM_2',
      'USM - CMA',
      'USM - MPB')) id,
  TIMESTAMP(offsetdate) offsetdate,
  TIMESTAMP(TIMESTAMP_TO_SEC(timestamp)-10800) timestamp,
  IF(DATEDIFF(timestamp,offsetdate)=0,' NO DIA',CONCAT ('D+ ',STRING (DATEDIFF (timestamp,offsetdate)))) defasagem,
  ROUND (MAX (pc),2) pc,
  ROUND (MAX (rh),2) rh,
  ROUND (MAX (tp),2) tp,
  ROUND (MAX (rd),2) rd,
  if (DATEDIFF (timestamp,offsetdate)=0," NO DIA",IF(DATEDIFF (timestamp,offsetdate)>=1 and DATEDIFF (timestamp,offsetdate)<=3, "D+ 1 à D+ 3","D+ 4 à D+ 6")) def_controle,
FROM (
  SELECT
    FIRST(SPLIT(f0, ';')) forecast_type,
    (NTH(2, SPLIT(f0, ';'))) id,
    TIMESTAMP(NTH(3, SPLIT(f0, ';'))) offsetdate,
    TIMESTAMP(NTH(4, SPLIT(f0, ';'))) timestamp,
    FLOAT(NTH(5, SPLIT(f0, ';'))) pc,
    FLOAT(NTH(6, SPLIT(f0, ';'))) rh,
    FLOAT(NTH(7, SPLIT(f0, ';'))) tp,
    FLOAT(NTH(8, SPLIT(f0, ';'))) rd,
  FROM
    [pluvion-tech:pluvion_lab.fct_serv])
WHERE
  pc IS NOT NULL
  AND DATEDIFF (timestamp,offsetdate)<=6 --Previsão D+0 até D+5
  AND forecast_type = 'sd10dm'
  AND id IN ('USM_2',
    'USM_3',
    'USM_5')
  --AND DATE(timestamp)=CURRENT_DATE()
  --Retira as duplicações, considerando o maior valor pluviometrico
GROUP BY
  forecast_type,
  id,
  offsetdate,
  timestamp,
  defasagem,
  def_controle,
ORDER BY
  offsetdate DESC,
  timestamp,
  id,