SELECT
  DISTINCT 
  fcCode ds_fcCode,
  DATE(TIMESTAMP(forecastedAtISO)) ds_forecastedAtISO,
  DATE(TIMESTAMP(timeISO)) ds_timeISO,
  latitude ds_latitude,
  longitude ds_longitude,
  stationId ds_stationId,
  SUM(precipIntensity) ds_precipIntensity,
  AVG(precipProbability) ds_precipProbability
FROM
  `pluvion-tech.pluvion_lab.fc_ds_168h_h_temporary`
GROUP BY
  ds_fcCode,
  ds_forecastedAtISO,
  ds_timeISO,
  ds_latitude,
  ds_longitude,
  ds_stationId