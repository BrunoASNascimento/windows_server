SELECT
  fcCode wc_fcCode,
  latitude wc_latitude,
  longitude wc_longitude,
  stationId wc_stationId,
  DATE(TIMESTAMP(forecastedAtISO)) wc_forecastedAtISO,
  DATE(TIMESTAMP(REPLACE(fcstValidLocal,"-0300","" ))) wc_fcstValidLocal,
  dayPop wc_dayPop,
  dayPopPhrase wc_dayPopPhrase,
  dayQpf wc_dayQpf,
  qpf wc_qpf
FROM
  `pluvion-tech.pluvion_lab.fc_wc_10d_d_temporary`