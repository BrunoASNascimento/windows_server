SELECT
  DISTINCT Lat wrf_lat,
  Lon wrf_lon,
  DATE(TIMESTAMP(date)) wrf_date,
  DATE(TIMESTAMP(prev)) wrf_prev,
  SUM(PWAT) wrf_PWAT,
  MAX(APCP) wrf_APCP,
  AVG(TMP) wrf_TMP,
  SUM(delta) wrf_delta
FROM
  `pluvion-tech.pluvion_lab.fc_inpe_wrf_4d`
WHERE
  lat IS NOT NULL
  AND delta IS NOT NULL
  AND SUBSTR( file_source,5,3) <> "cpt"
GROUP BY
  1,
  2,
  3,
  4