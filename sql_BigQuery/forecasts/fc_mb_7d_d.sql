SELECT
  fcCode mb_fcCode,
  date(TIMESTAMP(forecastedAtISO)) mb_forecastedAtISO,
  date(TIMESTAMP(time)) mb_time,
  latitude mb_latitude,
  longitude mb_longitude,
  convective_precipitation mb_convective_precipitation,
  precipitation mb_precipitation,
  precipitation_hours mb_precipitation_hours,
  precipitation_probability mb_precipitation_probability,
  predictability mb_predictability,
  predictability_class mb_predictability_class
FROM
  `pluvion-tech.pluvion_lab.fc_mb_7d_d_temporary`