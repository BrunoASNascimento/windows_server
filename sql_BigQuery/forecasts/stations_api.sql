SELECT  DISTINCT
  name stations_api_name,
  hwid stations_api_hwid,
  code stations_api_code,
  bcCode stations_api_bcCode,
  latitude_round stations_api_latitude_round,
  longitude_round stations_api_longitude_round,
  latitude_gfs stations_api_latitude_gfs,
  longitude_gfs stations_api_longitude_gfs,
  latitude_wrf stations_api_latitude_wrf,
  longitude_wrf stations_api_longitude_wrf
FROM
  `pluvion-tech.pluvion_lab.stations_api`