SELECT distinct 
  DATE(offsetdate) gfs_offsetdate,
  DATE(timestamp) gfs_timestamp,
  Lat gfs_Lat,
  Lon gfs_Lon,
  SUM(PWAT) gfs_PWAT,
  SUM(PRATE*21600) gfs_PRATE,
  SUM(APCP) gfs_APCP,
  AVG(TMP) gfs_TMP
FROM
  `pluvion-tech.pluvion_lab.fc_noaa_gfs`
WHERE
  MOD(EXTRACT( hour
    FROM
      timestamp),6)=0
  AND TMP IS NOT NULL
GROUP BY
  1,
  2,
  3,
  4