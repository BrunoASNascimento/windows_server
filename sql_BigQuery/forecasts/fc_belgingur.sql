SELECT
    DISTINCT forecastType,
    coord,
    lat,
    lon,
    stationHWID,
    name,
    DATE(forecastDate) forecastDate,
    DATE(hour) hour,
    SUM(pc) pc,
    COUNT(1) number_data
FROM (
    SELECT
        DISTINCT *
    FROM
        `pluvion-tech.pluvion_lab.fc_belgingur`)
  GROUP BY
    forecastType,
    coord,
    lat,
    lon,
    stationHWID,
    name,
    forecastDate,
    hour