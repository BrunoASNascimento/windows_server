SELECT
  DISTINCT bg_forecastType,
  bg_coord,
  bg_lat,
  bg_lon,
  bg_stationHWID,
  bg_name,
  stations_api_latitude_round,
  stations_api_longitude_round,
  bg_forecastDate,
  bg_hour,
  bg_pc,
  bg_number_data,
  ct_fcCode,
  ct_forecastedAtISO,
  ct_dateISO,
  ct_latitude,
  ct_longitude,
  ct_stationId,
  ct_rainPrecipitation,
  ct_rainProbability,
  ds_fcCode,
  ds_forecastedAtISO,
  ds_timeISO,
  ds_latitude,
  ds_longitude,
  ds_stationId,
  ds_precipIntensity,
  ds_precipProbability,
  mb_fcCode,
  mb_forecastedAtISO,
  mb_time,
  mb_latitude,
  mb_longitude,
  mb_convective_precipitation,
  mb_precipitation,
  mb_precipitation_hours,
  mb_precipitation_probability,
  mb_predictability,
  mb_predictability_class,
  wc_fcCode,
  wc_forecastedAtISO,
  wc_fcstValidLocal,
  wc_dayPop,
  wc_dayPopPhrase,
  wc_dayQpf,
  wc_qpf,
  stations_api_latitude_wrf,
  stations_api_longitude_wrf,
  wrf_APCP,
  stations_api_latitude_gfs,
  stations_api_longitude_gfs,
  gfs_PWAT,
  gfs_PRATE,
  gfs_APCP
FROM (
  SELECT
    DISTINCT *
  FROM (
    SELECT
      DISTINCT *
    FROM (
      SELECT
        *
      FROM (
        SELECT
          *
        FROM (
          SELECT
            *
          FROM (
            SELECT
              DISTINCT *
            FROM (
              SELECT
                DISTINCT forecastType bg_forecastType,
                coord bg_coord,
                lat bg_lat,
                lon bg_lon,
                stationHWID bg_stationHWID,
                name bg_name,
                DATE(forecastDate) bg_forecastDate,
                DATE(hour) bg_hour,
                SUM(pc) bg_pc,
                COUNT(1) bg_number_data
              FROM (
                SELECT
                  DISTINCT *
                FROM
                  `pluvion-tech.pluvion_lab.fc_belgingur`)
              GROUP BY
                bg_forecastType,
                bg_coord,
                bg_lat,
                bg_lon,
                bg_stationHWID,
                bg_name,
                bg_forecastDate,
                bg_hour) AS bg
            LEFT JOIN (
              SELECT
                DISTINCT DATE(TIMESTAMP(forecastedAtISO)) ct_forecastedAtISO,
                DATE(TIMESTAMP(dateISO)) ct_dateISO,
                fcCode ct_fcCode,
                latitude ct_latitude,
                longitude ct_longitude,
                stationId ct_stationId,
                rainPrecipitation ct_rainPrecipitation,
                rainProbability ct_rainProbability
              FROM
                `pluvion-tech.pluvion_lab.fc_ct_15d_d_temporary`
              ORDER BY
                ct_stationId,
                ct_forecastedAtISO,
                ct_dateISO) AS ct
            ON
              bg_stationHWID=ct_stationId
              AND bg_forecastDate=ct_forecastedAtISO
              AND bg_hour=ct_dateISO)
          LEFT JOIN (
            SELECT
              DISTINCT fcCode ds_fcCode,
              DATE(TIMESTAMP(forecastedAtISO)) ds_forecastedAtISO,
              DATE(TIMESTAMP(timeISO)) ds_timeISO,
              latitude ds_latitude,
              longitude ds_longitude,
              stationId ds_stationId,
              SUM(precipIntensity) ds_precipIntensity,
              AVG(precipProbability) ds_precipProbability
            FROM
              `pluvion-tech.pluvion_lab.fc_ds_168h_h_temporary`
            GROUP BY
              ds_fcCode,
              ds_forecastedAtISO,
              ds_timeISO,
              ds_latitude,
              ds_longitude,
              ds_stationId)
          ON
            bg_stationHWID=ds_stationId
            AND bg_forecastDate=ds_forecastedAtISO
            AND bg_hour=ds_timeISO)
        LEFT JOIN (
          SELECT
            DISTINCT fcCode mb_fcCode,
            DATE(TIMESTAMP(forecastedAtISO)) mb_forecastedAtISO,
            DATE(TIMESTAMP(time)) mb_time,
            latitude mb_latitude,
            longitude mb_longitude,
            convective_precipitation mb_convective_precipitation,
            precipitation mb_precipitation,
            precipitation_hours mb_precipitation_hours,
            precipitation_probability mb_precipitation_probability,
            predictability mb_predictability,
            predictability_class mb_predictability_class
          FROM
            `pluvion-tech.pluvion_lab.fc_mb_7d_d_temporary`)AS mb
        ON
          ROUND(bg_lat,4)=ROUND(mb_latitude,4)
          AND ROUND(bg_lon,4)=ROUND(mb_longitude,4)
          AND bg_forecastDate=mb_forecastedAtISO
          AND bg_hour=mb_time)
      LEFT JOIN (
        SELECT
          DISTINCT fcCode wc_fcCode,
          latitude wc_latitude,
          longitude wc_longitude,
          stationId wc_stationId,
          DATE(TIMESTAMP(forecastedAtISO)) wc_forecastedAtISO,
          DATE(TIMESTAMP(REPLACE(fcstValidLocal,"-0300","" ))) wc_fcstValidLocal,
          dayPop wc_dayPop,
          dayPopPhrase wc_dayPopPhrase,
          dayQpf wc_dayQpf,
          qpf wc_qpf
        FROM
          `pluvion-tech.pluvion_lab.fc_wc_10d_d_temporary`) AS wc
      ON
        bg_stationHWID=wc_stationId
        AND bg_forecastDate=wc_forecastedAtISO
        AND bg_hour=wc_fcstValidLocal)
    LEFT JOIN (
      SELECT
        DISTINCT name stations_api_name,
        hwid stations_api_hwid,
        code stations_api_code,
        bcCode stations_api_bcCode,
        latitude_round stations_api_latitude_round,
        longitude_round stations_api_longitude_round,
        latitude_gfs stations_api_latitude_gfs,
        longitude_gfs stations_api_longitude_gfs,
        latitude_wrf stations_api_latitude_wrf,
        longitude_wrf stations_api_longitude_wrf
      FROM
        `pluvion-tech.pluvion_lab.stations_api`)
    ON
      bg_stationHWID=stations_api_hwid)
  LEFT JOIN (
    SELECT
      DISTINCT Lat wrf_lat,
      Lon wrf_lon,
      DATE(TIMESTAMP(date)) wrf_date,
      DATE(TIMESTAMP(prev)) wrf_prev,
      SUM(PWAT) wrf_PWAT,
      MAX(APCP) wrf_APCP,
      AVG(TMP) wrf_TMP,
      SUM(delta) wrf_delta
    FROM
      `pluvion-tech.pluvion_lab.fc_inpe_wrf_4d`
    WHERE
      lat IS NOT NULL
      AND delta IS NOT NULL
      AND SUBSTR( file_source,5,3) <> "cpt"
    GROUP BY
      1,
      2,
      3,
      4)AS wrf
  ON
    stations_api_latitude_wrf=wrf_lat
    AND stations_api_longitude_wrf=wrf_lon
    AND bg_forecastDate=wrf_date
    AND bg_hour=wrf_prev)
LEFT JOIN (
  SELECT
    DISTINCT DATE(offsetdate) gfs_offsetdate,
    DATE(timestamp) gfs_timestamp,
    Lat gfs_Lat,
    Lon gfs_Lon,
    SUM(PWAT) gfs_PWAT,
    SUM(PRATE*21600) gfs_PRATE,
    SUM(APCP) gfs_APCP,
    AVG(TMP) gfs_TMP
  FROM
    `pluvion-tech.pluvion_lab.fc_noaa_gfs`
  WHERE
    MOD(EXTRACT( hour
      FROM
        timestamp),6)=0
    AND TMP IS NOT NULL
  GROUP BY
    1,
    2,
    3,
    4)
ON
  stations_api_latitude_gfs=gfs_Lat
  AND stations_api_longitude_gfs=gfs_Lon
  AND bg_forecastDate=gfs_offsetdate
  AND bg_hour=gfs_timestamp