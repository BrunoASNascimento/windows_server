SELECT
  fcCode,
  DISTINCT DATE(TIMESTAMP(forecastedAtISO)) forecastedAtISO,
  DATE(TIMESTAMP(dateISO)) dateISO,  
  latitude,
  longitude,
  stationId,
  rainPrecipitation,
  rainProbability
FROM
  `pluvion-tech.pluvion_lab.fc_ct_15d_d_temporary`
ORDER BY
  stationId,
  forecastedAtISO,
  dateISO