SELECT
  fcCode,
  latitude,
  longitude,
  forecastedAtISO,
  parsedAtISO,
  --cast((SUBSTR( currentDay,6,17))as DATE )currentDay,
  concat(SUBSTR( currentDay,6,17))currentDay,
  substr( dateOrig,6,17) dateOrig,
  CAST((REPLACE(currentPrecipitation,"mm","")) AS float64) currentPrecipitation,
  CAST((REPLACE(accPrecipitation,"mm",""))AS float64) accPrecipitation,
  CAST((REPLACE(rainProbability,"%",""))AS float64) rainProbability
FROM
  `pluvion-tech.pluvion_lab.fc_smp_15d_d_temporary`