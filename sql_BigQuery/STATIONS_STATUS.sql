SELECT
  a.code,
  a.HWID,
  IF(DATE(b.data_max) = CURRENT_DATE(), 'Online','Offline') status_today,
FROM
  [pluvion-tech:pluvion_lab.stations_list_view] AS a
LEFT JOIN (
  SELECT
    hwid,
    MAX(measuredAtISO) data_max,
  FROM
    [pluvion-tech:pluvion_lab.msg_mgmt]
  GROUP BY
    hwid,
    ) AS b
ON
  b.hwid=a.HWID