SELECT
  CONCAT(string (DAY(measuredAtiso)),"/",string (MONTH(measuredAtiso)),"/",string (YEAR(measuredAtiso))) Data,
  if (hwid ='bcce5f04466c',
    'i41',
    if (hwid ='9915e3231ada',
      'i42',
      ' CivilMaster RJ')) NOME_DESCRICAO,
  AVG(pcVol) pc_obs,
  AVG(atpAvg) temp_obs,
FROM
  [pluvion-tech:pluvion_dev.msg_pc],
  [pluvion-tech:pluvion_dev.msg_atp],
  [pluvion-tech:pluvion_dev.msg_arh],
  [pluvion-tech:pluvion_lab.msg_ws],
  [pluvion-tech:pluvion_lab.msg_wd]
WHERE
  hwid IN ('9915e3231ada',
    'bcce5f04466c',
    '41C500')
GROUP BY
  Data,
  measuredAtiso,
  hwid,
  NOME_DESCRICAO,
ORDER BY
  measuredAtISO DESC,
  hwid