SELECT
  *
FROM (
  SELECT
    FIRST(SPLIT(f0, ';')) type,
    FLOAT(NTH(2, SPLIT(f0, ';'))) number,
    TIMESTAMP(NTH(3, SPLIT(f0, ';'))) createdAtISO,
    (NTH(4, SPLIT(f0, ';'))) neighbour,
    NTH(5, SPLIT(f0, ';')) address,
    NTH(6, SPLIT(f0, ';')) modelId,
    NTH(7, SPLIT(f0, ';')) bcCode,
    NTH(8, SPLIT(f0, ';')) status,
    FLOAT(NTH(9, SPLIT(f0, ';'))) latitude,
    (NTH(10, SPLIT(f0, ';'))) name,
    TIMESTAMP(NTH(11, SPLIT(f0, ';'))) updatedAtISO,
    (NTH(12, SPLIT(f0, ';'))) bluetoothId,
    (NTH(13, SPLIT(f0, ';'))) category,
    (NTH(14, SPLIT(f0, ';'))) satelliteId,
    (NTH(15, SPLIT(f0, ';'))) esimId,
    (NTH(16, SPLIT(f0, ';'))) updatedAt,
    (NTH(17, SPLIT(f0, ';'))) city,
    (NTH(18, SPLIT(f0, ';'))) complement,
    FLOAT(NTH(19, SPLIT(f0, ';'))) longitude,
    (NTH(20, SPLIT(f0, ';'))) stId,
    (NTH(21, SPLIT(f0, ';'))) wifiId,
    (NTH(22, SPLIT(f0, ';'))) hwid,
    (NTH(23, SPLIT(f0, ';'))) description,
    (NTH(24, SPLIT(f0, ';'))) code,
    (NTH(25, SPLIT(f0, ';'))) zipCode,
    (NTH(26, SPLIT(f0, ';'))) sigfoxId,
    (NTH(27, SPLIT(f0, ';'))) state,
    (NTH(28, SPLIT(f0, ';'))) loraId,
    (NTH(29, SPLIT(f0, ';'))) nbId,
    (NTH(30, SPLIT(f0, ';'))) gsmId,
    (NTH(31, SPLIT(f0, ';'))) createdAt,
    (NTH(32, SPLIT(f0, ';'))) fmwId,
    (NTH(33, SPLIT(f0, ';'))) country, 
  FROM
    [pluvion-tech:pluvion_lab.stations_2])
WHERE
  LATITUDE IS NOT NULL