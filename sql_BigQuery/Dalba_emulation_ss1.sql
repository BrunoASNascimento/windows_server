SELECT
  measuredAtiso server_date,
  if (hwid ='bcce5f04466c',
    'i41',
    'i42') id,
  measuredAt device_timestamp,
  ((AVG(pcVol))/0.255) tips,
  float('-') Radiation,
  AVG(atpAvg) temp,
  AVG(arhAvg) rh,
  float('-') signal,
  float('-') CRC16,
FROM
  [pluvion-tech:pluvion_dev.msg_pc],
  [pluvion-tech:pluvion_dev.msg_atp],
  [pluvion-tech:pluvion_dev.msg_arh],
  [pluvion-tech:pluvion_lab.msg_ws],
  [pluvion-tech:pluvion_lab.msg_wd]
WHERE
  hwid IN ('9915e3231ada',
    'bcce5f04466c')
GROUP BY
  measuredAtiso,
  server_date,
  device_timestamp,
  id,
  measuredAt,
  Radiation,
  signal,
  CRC16,
  hwid,
ORDER BY
  measuredAtISO DESC,
  hwid