SELECT
  *
FROM (
  SELECT
    FIRST(SPLIT(string_field_0, ';')) name,
    NTH(2, SPLIT(string_field_0, ';')) sigfoxId,
    FLOAT(NTH(3, SPLIT(string_field_0, ';'))) latitude,
    FLOAT(NTH(4, SPLIT(string_field_0, ';'))) longitude,
    (NTH(5, SPLIT(string_field_0, ';'))) status,
    (NTH(6, SPLIT(string_field_0, ';'))) cemaden,
    FLOAT(NTH(7, SPLIT(string_field_0, ';'))) lat_cemaden,
    FLOAT(NTH(8, SPLIT(string_field_0, ';'))) lon_cemaden,
    (NTH(9, SPLIT(string_field_0, ';'))) saisp,
    FLOAT(NTH(10, SPLIT(string_field_0, ';'))) lat_saisp,
    FLOAT(NTH(11, SPLIT(string_field_0, ';'))) lon_saisp,
    (NTH(12, SPLIT(string_field_0, ';'))) inmet,
    FLOAT(NTH(13, SPLIT(string_field_0, ';'))) lat_inmet,
    FLOAT(NTH(14, SPLIT(string_field_0, ';'))) lon_inmet,
    FLOAT(NTH(15, SPLIT(string_field_0, ';'))) distancia_m,
  FROM
    [pluvion-tech:pluvion_lab.estac123])
WHERE
  distancia_m IS NOT NULL