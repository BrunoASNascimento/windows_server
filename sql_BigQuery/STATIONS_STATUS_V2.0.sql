SELECT
  c.hwid hwid,
  c.name name,
  c.data data,
  c.firmwareVersion firmwareVersion,
  c.rainSensorHasError rainSensorHasError,
  c.temperatureSensorHasError temperatureSensorHasError,
  c.humiditySensorHasError humiditySensorHasError,
  c.windSpeedSensorHasError windSpeedSensorHasError,
  c.windDirectionSensorHasError windDirectionSensorHasError,
  c.irradiationSensorHasError irradiationSensorHasError,
  c.voltageSensorHasError voltageSensorHasError,
  c.batteryPercentage batteryPercentage,
  c.batteryMinutesOfUsage batteryMinutesOfUsage,
  c.inputVoltage inputVoltage,
  c.data_controle data_controle,
  c.today today,
  c.status status,
  IF(DATE(d.data_max) = CURRENT_DATE(), 'Online','Offline') situation,
FROM (
  SELECT
    a.hwid hwid,
    b.code name,
    a.data data,
    a.firmwareVersion firmwareVersion,
    a.rainSensorHasError rainSensorHasError,
    a.temperatureSensorHasError temperatureSensorHasError,
    a.humiditySensorHasError humiditySensorHasError,
    a.windSpeedSensorHasError windSpeedSensorHasError,
    a.windDirectionSensorHasError windDirectionSensorHasError,
    a.irradiationSensorHasError irradiationSensorHasError,
    a.voltageSensorHasError voltageSensorHasError,
    a.batteryPercentage batteryPercentage,
    a.batteryMinutesOfUsage batteryMinutesOfUsage,
    a.inputVoltage inputVoltage,
    DATE(data) data_controle,
    CURRENT_DATE() today,
    b.status
  FROM (
    SELECT
      hwid,
      MAX(measuredAtISO) data,
      firmwareVersion,
      rainSensorHasError,
      temperatureSensorHasError,
      humiditySensorHasError,
      windSpeedSensorHasError,
      windDirectionSensorHasError,
      irradiationSensorHasError,
      voltageSensorHasError,
      IF(ROUND(batteryPercentage,1)=127,100,ROUND(batteryPercentage,1)) batteryPercentage,
      ROUND(batteryMinutesOfUsage,1) batteryMinutesOfUsage,
      ROUND(inputVoltage,1) inputVoltage
    FROM
      [pluvion-tech:pluvion_dev.msg_mgmt]
    WHERE
      rainSensorHasError IS TRUE
      OR temperatureSensorHasError IS TRUE
      OR humiditySensorHasError IS TRUE
      OR windSpeedSensorHasError IS TRUE
      OR windDirectionSensorHasError IS TRUE
      OR irradiationSensorHasError IS TRUE
      OR voltageSensorHasError IS TRUE
    GROUP BY
      hwid,
      firmwareVersion,
      rainSensorHasError,
      temperatureSensorHasError,
      humiditySensorHasError,
      windSpeedSensorHasError,
      windDirectionSensorHasError,
      irradiationSensorHasError,
      voltageSensorHasError,
      batteryPercentage,
      batteryPercentage,
      batteryMinutesOfUsage,
      inputVoltage
    ORDER BY
      data DESC,
      hwid)AS a
  LEFT JOIN
    [pluvion-tech:pluvion_lab.stations_list_view] AS b
  ON
    a.hwid = b.HWID
  ORDER BY
    data DESC)AS c
LEFT JOIN (
  SELECT
    hwid,
    MAX(measuredAtISO) data_max,
  FROM
    [pluvion-tech:pluvion_lab.msg_atp],
    [pluvion-tech:pluvion_lab.msg_wd],
    [pluvion-tech:pluvion_lab.msg_pc]
  GROUP BY
    hwid,
    ) AS d
ON
  c.hwid=d.hwid
ORDER BY
  name,
  data DESC