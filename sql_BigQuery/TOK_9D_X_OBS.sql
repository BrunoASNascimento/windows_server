SELECT
  c.createdAt,
  c.fcDate,
  c.source,
  c.resolution,
  c.period,
  c.periodicity,
  c.extra,
  c.code,
  c.stationID,
  c.latitude,
  c.longitude,
  c.pcDailyAcc prev,
  c.tpDailyMin,
  c.tpDailyMax,
  c.tpDailyAvg,
  c.rhDailyMin,
  c.rhDailyMax,
  c.rhDailyAvg,
  c.wsDailyMin,
  c.wsDailyMax,
  c.wsDailyAvg,
  c.msg_repetidas,
  c.HWID,
  d.pcVol obs,
  (c.pcDailyAcc-d.pcVol) dif,
  boolean (c.pcDailyAcc>0) bollean_prev,
  BOOLEAN(d.pcVol>0) bollean_obs,
  BOOLEAN(c.pcDailyAcc>0 AND d.pcVol>0) prevXobs
FROM (
  SELECT
    a.createdAt createdAt,
    a.fcDate fcDate,
    a.source source,
    a.resolution resolution,
    a.period period,
    a.periodicity periodicity,
    a.extra extra,
    a.code code,
    a.stationID stationID,
    a.latitude latitude,
    a.longitude longitude,
    a.pcDailyAcc pcDailyAcc,
    a.tpDailyMin tpDailyMin,
    a.tpDailyMax tpDailyMax,
    a.tpDailyAvg tpDailyAvg,
    a.rhDailyMin rhDailyMin,
    a.rhDailyMax rhDailyMax,
    a.rhDailyAvg rhDailyAvg,
    a.wsDailyMin wsDailyMin,
    a.wsDailyMax wsDailyMax,
    a.wsDailyAvg wsDailyAvg,
    a.msg_repetidas msg_repetidas,
    b.HWID HWID,
  FROM (
    SELECT
      DATE(createdAt) createdAt,
      DATE(fcDate) fcDate,
      source,
      resolution,
      period,
      periodicity,
      extra,
      code,
      stationID,
      latitude,
      longitude,
      AVG(pcDailyAcc) pcDailyAcc,
      AVG(tpDailyMin) tpDailyMin,
      AVG(tpDailyMax) tpDailyMax,
      AVG(tpDailyAvg) tpDailyAvg,
      AVG(rhDailyMin) rhDailyMin,
      AVG(rhDailyMax) rhDailyMax,
      AVG(rhDailyAvg) rhDailyAvg,
      AVG(wsDailyMin) wsDailyMin,
      AVG(wsDailyMax) wsDailyMax,
      AVG(wsDailyAvg) wsDailyAvg,
      COUNT(1) msg_repetidas
    FROM
      [pluvion-tech:pluvion_lab.fc_tok_ld_9_d_gfs]
    GROUP BY
      1,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9,
      10,
      11) AS a
  LEFT JOIN
    [pluvion-tech:pluvion_lab.stations_list_view] AS b
  ON
    a.stationID=b.code) AS c
LEFT JOIN (
  SELECT
    hwid,
    DATE(measuredAtISO) measuredAtISO,
    SUM(pcVol) pcVol
  FROM
    [pluvion-tech:pluvion_lab.msg_pc]
  GROUP BY
    1,
    2 ) AS d
ON
  c.HWID=d.hwid
  AND c.fcDate=d.measuredAtISO