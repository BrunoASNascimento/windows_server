SELECT
  forecast_type,
  IF(id='DALBA_1','Pedreira Guarapuava','PCH Rio Jordão') id,
  TIMESTAMP(offsetdate) offsetdate,
  TIMESTAMP(TIMESTAMP_TO_SEC(timestamp)-10800) timestamp,
  IF(DATEDIFF(timestamp,offsetdate)=0,' NO DIA',CONCAT ('D+ ',STRING (DATEDIFF (timestamp,offsetdate)))) defasagem,
  IF( (HOUR(timestamp)-3)>=13
    AND (HOUR(timestamp)-3)<18,' [13h - 18h] - Vespertino',IF( (HOUR(timestamp)-3)>=6
      AND (HOUR(timestamp)-3)<13,'  [6h - 13h] - Manhã','[18h - 6h] - Noite')) hour,
  ROUND (MAX (pc),2) pc,
FROM (
  SELECT
    FIRST(SPLIT(f0, ';')) forecast_type,
    (NTH(2, SPLIT(f0, ';'))) id,
    TIMESTAMP(NTH(3, SPLIT(f0, ';'))) offsetdate,
    TIMESTAMP(NTH(4, SPLIT(f0, ';'))) timestamp,
    FLOAT(NTH(5, SPLIT(f0, ';'))) pc,
  FROM
    [pluvion-tech:pluvion_lab.fct_serv])
WHERE
  pc IS NOT NULL
  AND DATEDIFF (timestamp,offsetdate)<=5 --Previsão D+0 até D+5
  AND forecast_type = 'sd10d'
  AND id IN ('DALBA_1','DALBA_2')
  --AND DATE(timestamp)=CURRENT_DATE()
  --Retira as duplicações, considerando o maior valor pluviometrico
GROUP BY
  forecast_type,
  id,
  offsetdate,
  timestamp,
  defasagem,
  hour,
ORDER BY
  id,
  offsetdate DESC,
  timestamp,