SELECT
  hwid,
  if (hwid ='8f0ca6dee128',
    'USM - Ibitirama',
    if (hwid ='681a7a10dce5',
      'USM - CMA',
      'USM - MPB')) nome_descricao,
  rawHex,
  measuredAt,
  measuredAtiso,
  TIMESTAMP(measuredAt-10800) measuredAtiso_3h,
  --tira 3h
  AVG(if(pcVol is null,0,pcVol)) pcV1,
  AVG(atpMin) atpMin,
  AVG(atpMax) atpMax,
  AVG(atpAvg) atpAvg,
  AVG(arhMin) arhMin,
  AVG(arhMax) arhMax,
  AVG(arhAvg) arhAvg,
  3.6*(AVG(wsMax)) wsMax,
  3.6*(AVG(wsVecAvg)) wsVecAvg,
  3.6*(AVG(wsL5mAvg))wsL5mAvg,
  AVG(wdMaxS) wdMaxS,
  AVG(wdVecAvg) wdVecAvg,
  if(AVG(rdMin)=30 or hwid <> '8f0ca6dee128',0,AVG(2.38*rdMin)) rdMin,
  if(AVG(rdMax)=30 or hwid <> '8f0ca6dee128',0,AVG(2.38*rdMax)) rdMax,
  if(AVG(rdAvg)=30 or hwid <> '8f0ca6dee128',0,AVG(2.38*rdAvg)) rdAvg,
  COUNT(1) contagem
FROM
  [pluvion-tech:pluvion_dev.msg_pc],
  [pluvion-tech:pluvion_dev.msg_atp],
  [pluvion-tech:pluvion_dev.msg_arh],
  [pluvion-tech:pluvion_lab.msg_ws],
  [pluvion-tech:pluvion_lab.msg_wd],
  [pluvion-tech:pluvion_lab.msg_rd]
WHERE
  hwid IN ('8f0ca6dee128',
    '681a7a10dce5',
    'ed2aa244c794') 
GROUP BY
  hwid,
  rawHex,
  nome_descricao,
  measuredAt,
  measuredAtiso,
  measuredAtiso_3h,
ORDER BY
  measuredAtISO DESC,
  hwid