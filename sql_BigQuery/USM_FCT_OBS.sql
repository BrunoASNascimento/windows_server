SELECT
  *
FROM (
  SELECT
    forecast_type,
    if (id ='USM_5',
      'USM - Ibitirama',
      if (id ='USM_2',
        'USM - CMA',
        'USM - MPB')) id,
    TIMESTAMP(offsetdate) offsetdate,
    TIMESTAMP(TIMESTAMP_TO_SEC(timestamp)-10800) timestamp,
    IF(DATEDIFF(timestamp,offsetdate)=0,' NO DIA',CONCAT ('D+ ',STRING (DATEDIFF (timestamp,offsetdate)))) defasagem,
    ROUND (MAX (pc),2) pc,
    ROUND (MAX (rh),2) rh,
    ROUND (MAX (tp),2) tp,
    ROUND (MAX (rd),2) rd,
    if (DATEDIFF (timestamp,offsetdate)=0,
      " NO DIA",
      IF(DATEDIFF (timestamp,offsetdate)>=1
        AND DATEDIFF (timestamp,offsetdate)<=3, "D+ 1 à D+ 3","D+ 4 à D+ 6")) def_controle,
  FROM (
    SELECT
      FIRST(SPLIT(f0, ';')) forecast_type,
      (NTH(2, SPLIT(f0, ';'))) id,
      TIMESTAMP(NTH(3, SPLIT(f0, ';'))) offsetdate,
      TIMESTAMP(NTH(4, SPLIT(f0, ';'))) timestamp,
      FLOAT(NTH(5, SPLIT(f0, ';'))) pc,
      FLOAT(NTH(6, SPLIT(f0, ';'))) rh,
      FLOAT(NTH(7, SPLIT(f0, ';'))) tp,
      FLOAT(NTH(8, SPLIT(f0, ';'))) rd,
    FROM
      [pluvion-tech:pluvion_lab.fct_serv])
  WHERE
    pc IS NOT NULL
    AND DATEDIFF (timestamp,offsetdate)<=6 --Previsão D+0 até D+5
    AND forecast_type = 'sd10dm'
    AND id IN ('USM_2',
      'USM_3',
      'USM_5')
    --AND DATE(timestamp)=CURRENT_DATE()
    --Retira as duplicações, considerando o maior valor pluviometrico
  GROUP BY
    forecast_type,
    id,
    offsetdate,
    timestamp,
    defasagem,
    def_controle,
  ORDER BY
    offsetdate DESC,
    timestamp,
    id) AS prev
LEFT JOIN (
  SELECT
    hwid,
    nome_descricao,
    TIMESTAMP(CONCAT(STRING(DATE(measuredAtiso)),' ',STRING(HOUR(measuredAtiso)),':00:00')) measuredAtiso,
    TIMESTAMP(CONCAT(STRING(DATE(measuredAtiso_3h)),' ',STRING(HOUR(measuredAtiso_3h)),':00:00')) measuredAtiso_3h,
    SUM(pcV1) pcV1,
    MIN(atpMin) atpMin,
    MAX(atpMax) atpMax,
    AVG(atpAvg) atpAvg,
    MIN(arhMin) arhMin,
    MAX(arhMax) arhMax,
    AVG(arhAvg) arhAvg,
    MAX(wsMax) wsMax,
    AVG(wsVecAvg) wsVecAvg,
    AVG(wsL5mAvg) wsL5mAvg,
    MIN(rdMin) rdMin,
    MAX(rdMax) rdMax,
    AVG(rdAvg) rdAvg,
  FROM (
    SELECT
      hwid,
      if (hwid ='8f0ca6dee128',
        'USM - Ibitirama',
        if (hwid ='681a7a10dce5',
          'USM - CMA',
          'USM - MPB')) nome_descricao,
      rawHex,
      measuredAt,
      measuredAtiso,
      TIMESTAMP(measuredAt-10800) measuredAtiso_3h,
      --tira 3h
      AVG(IF(pcVol IS NULL,0,pcVol)) pcV1,
      AVG(atpMin) atpMin,
      AVG(atpMax) atpMax,
      AVG(atpAvg) atpAvg,
      AVG(arhMin) arhMin,
      AVG(arhMax) arhMax,
      AVG(arhAvg) arhAvg,
      3.6*(AVG(wsMax)) wsMax,
      3.6*(AVG(wsVecAvg)) wsVecAvg,
      3.6*(AVG(wsL5mAvg))wsL5mAvg,
      AVG(wdMaxS) wdMaxS,
      AVG(wdVecAvg) wdVecAvg,
      IF(AVG(rdMin)=30
        OR hwid <> '8f0ca6dee128',0,AVG(2.38*rdMin)) rdMin,
      IF(AVG(rdMax)=30
        OR hwid <> '8f0ca6dee128',0,AVG(2.38*rdMax)) rdMax,
      IF(AVG(rdAvg)=30
        OR hwid <> '8f0ca6dee128',0,AVG(2.38*rdAvg)) rdAvg,
      COUNT(1) contagem
    FROM
      [pluvion-tech:pluvion_dev.msg_pc],
      [pluvion-tech:pluvion_dev.msg_atp],
      [pluvion-tech:pluvion_dev.msg_arh],
      [pluvion-tech:pluvion_lab.msg_ws],
      [pluvion-tech:pluvion_lab.msg_wd],
      [pluvion-tech:pluvion_lab.msg_rd]
    WHERE
      hwid IN ('8f0ca6dee128',
        '681a7a10dce5',
        'ed2aa244c794')
    GROUP BY
      hwid,
      rawHex,
      nome_descricao,
      measuredAt,
      measuredAtiso,
      measuredAtiso_3h)
  GROUP BY
    1,
    2,
    3,
    4) AS obs
ON
  prev.id= obs.nome_descricao
  AND prev.timestamp=obs.measuredAtiso_3h