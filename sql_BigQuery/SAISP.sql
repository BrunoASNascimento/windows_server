SELECT
  a.posto posto,
  a.data data,
  a.lat lat,
  a.lon lon,
  round((IF((b.pcv1-a.pcV1)<0,0,(b.pcv1-a.pcV1))),3) pcV1
FROM
  [pluvion-tech:pluvion_lab.DB_SAISP_F] a
LEFT JOIN (
  SELECT
    posto,
    data,
    pcV1,
    (data-10*60*1000000) delta_data
  FROM
    [pluvion-tech:pluvion_lab.DB_SAISP_F]) b
ON
  a.posto = b.posto
  AND a.data = b.delta_data
ORDER BY
  a.posto,
  a.data