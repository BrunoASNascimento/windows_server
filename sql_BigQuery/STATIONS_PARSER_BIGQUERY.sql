SELECT
  STRING ( string_field_0 ) stId, --Id auto-gerado da estação
  STRING ( string_field_1 ) code, --Código da estação (ex.: PLUVION_12345)
  STRING ( string_field_2 ) bcCode, --Código de broadcast da estação (ex.: NIC_01)
  STRING ( string_field_3 ) HWID, --Id de Hardware (ex.: 12345, AE:EA:BC:DF:CS, 41C134,...)
  STRING ( string_field_4 ) name,   --Nome da estação (ex.: Praça Cazuza)
  STRING ( string_field_5 ) description, --Descrição da estação (ex.: Estação que está na praça)
  STRING ( string_field_6 ) status, --Descrição da estação (ex.: Estação que está na praça)
  STRING ( string_field_7 ) bluetoothId, --Id Bluetooth
  STRING ( string_field_8 ) sigfoxId, --Id Sigfox
  STRING ( string_field_9 ) gsmId, --Id GSM
  STRING ( string_field_10 ) wifiId, --Id WiFi
  STRING ( string_field_11 ) loraId, --Id LoRa
  STRING ( string_field_12 ) esimId, --Id do módulo eSIM
  STRING ( string_field_13 ) nbId, --Id do módulo NB
  STRING ( string_field_14 ) satelliteId, --Id do módulo de satéllite
  STRING ( string_field_15 ) decoder, --Código do decoder da mensagem
  STRING ( string_field_16 ) parser, --Código do parser que sabe interpretar o output do decoder
  STRING ( string_field_17 ) type, --Código do parser que sabe interpretar o output do decoder
  STRING ( string_field_18 ) fmwId, --Id do firmware utilizado por essa estação
  STRING ( string_field_19 ) fmwUpToDate, --Indica se há atualizações para esse firmware (esse campo é atualizado toda vez que o firmware sofrer alterações, true quando a estação é cadastrada)
  INTEGER ( string_field_20 ) fmwLastUpdate, --A última vez que o firmware dessa estação foi atualizado (data do upload do firmware para a estação)
  TIMESTAMP ( string_field_21 ) fmwLastUpdateISO, --A última vez que o firmware dessa estação foi atualizado (data do upload do firmware para a estação, formato ISO)
  FLOAT ( string_field_22 ) latitude, --Latitude
  FLOAT ( string_field_23 ) longitude, --Longitude
  STRING ( string_field_24 ) address, --Endereço
  STRING ( string_field_25 ) number, --Número
  STRING ( string_field_26 ) zipcode, --CEP
  STRING ( string_field_27 ) neighbour, --Bairro
  STRING ( string_field_28 ) city, --Cidade
  STRING ( string_field_29 ) state, --Estado
  STRING ( string_field_30 ) country, --País
  INTEGER ( string_field_31 ) dstOffset, --the offset for daylight-savings time in seconds. This will be zero if the time zone is not in Daylight Savings Time during the specified timestamp.
  INTEGER ( string_field_32 ) rawOffset, --the offset from UTC (in seconds) for the given location. This does not take into effect daylight savings.
  STRING ( string_field_33 ) timeZoneId, --Id do timezone ex: 'America/Los_Angeles' or 'Australia/Sydney' - https://maps.googleapis.com/maps/api/timezone/outputFormat?parameters
  STRING ( string_field_34 ) timeZoneName, --Id do timezone ex: 'America/Los_Angeles' or 'Australia/Sydney' - https://maps.googleapis.com/maps/api/timezone/outputFormat?parameters
  INTEGER ( string_field_35 ) timeZoneLastUpdate,--A última vez que o firmware dessa estação foi atualizado (necessário atualização periódica)
  TIMESTAMP ( string_field_36 ) timeZoneLastUpdateISO, --A última vez que o firmware dessa estação foi atualizado (formato ISO)
  INTEGER ( string_field_37 ) createdAt, --Data da criação
  TIMESTAMP ( string_field_38 ) createdAtISO, --Data da criação (formato ISO)
  STRING ( string_field_39 ) createdBy, --Id do usuário que criou
  INTEGER ( string_field_40 ) updatedAt, --Data da última atualização
  TIMESTAMP ( string_field_41 ) updatedAtISO, --Data da última atualização (formato ISO)
  STRING ( string_field_42 ) updatedBy, --Id do usuário que atualizou
  STRING ( string_field_43 ) comments, --Campo livre de comentário
  STRING ( string_field_44 ) meta, --Campo livre formato JSON para adicionar eventuais dados que não estejam no modelo
  STRING ( string_field_45 ) tags, --Campo livre de tags separadas por virgula
FROM
  [pluvion-tech:pluvion_lab.stations_V150219]
WHERE
  string_field_0 ='-'