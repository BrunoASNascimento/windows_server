SELECT
  sigfoxId,
  data,
  ROUND(SUM(pcV1_sum),2) pcV1_sum,
  IF((ROUND(SUM(pcV1_cont),2))>1,'C/Chuva','S/Chuva') pcV1_cont,
  IF((ROUND(SUM(atpAvg1_cont),2))=24,'OK','ERRO') atpAvg1_cont,
  IF((ROUND(SUM(arhAvg1_cont),2))=24,'OK','ERRO') arhAvg1_cont,
  IF((ROUND(SUM(wsVAvg1_cont),2))=24,'OK','ERRO') wsVAvg1_cont,
FROM (
  SELECT
    sigfoxId,
    DATE(measuredAtISO ) data,
    SUM(pcV1) pcV1_sum,
	COUNT(pcV1) pcV1_cont,
    COUNT(atpAvg1) atpAvg1_cont,
    COUNT(arhAvg1) arhAvg1_cont,
    COUNT(wsVAvg1) wsVAvg1_cont
  FROM
    [pluvion-tech:pluvion_dev.wtr_parsed]
  WHERE
    sigfoxId IN ('41C514','41D2C7','41C533','41C523','41C48F','41C47E','41C488','41C48B','41BF05','41C496','41C516','41C484','41C517','41C493','41C539','41C48A','41C505','41C51A','41C47D','41C527','41CD05','41C59F','41C486','41D2B0')
    AND msgCode IN ('ws_wd_ard','pc','at_arh')
    --and  measuredAt between 1548435906 and 1600000000
  GROUP BY
    sigfoxId,
    data )
GROUP BY
  sigfoxId,
  data
ORDER BY
  data,
  sigfoxId