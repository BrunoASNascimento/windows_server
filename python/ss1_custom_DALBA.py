from pymongo import MongoClient
import ssl
import datetime
import pytz
from ftplib import FTP
import os

MONGO_URL = os.environ.get(
    'MONGO_URL',
    'Specified environment variable is not set.'
)

utcmoment_naive = datetime.datetime.utcnow()
utcmoment = utcmoment_naive.replace(tzinfo=pytz.utc)

yesterday = (utcmoment - datetime.timedelta(hours=32) -
             datetime.timedelta(minutes=10))
filename = 'ss1_' + datetime.datetime.utcnow().strftime("%Y_%m_%d_%H") + '.csv'
stations = ['41', '42', '43']

start = yesterday
end = utcmoment
# print(start)
# print(end)
# convert your date string to datetime object
# start = datetime.datetime(2018, 8, 9, 0, 0, 00)
# end =   datetime.datetime(2018, 6, 17, 0, 0, 00)
# stations = ['31','32','33','34','35']

client = MongoClient(MONGO_URL, ssl_cert_reqs=ssl.CERT_NONE)
db = client['api-wtr']
cursor = db.weather.find({'createdAt': {'$lt': end, '$gte': start}, 'stationHWID': {
                         '$in': stations}}).sort([['_id', 1]])
# cursor = db.weather.find({'createdAt':{'$lt': end, '$gte': start},'stationHWID':{'$in':['98']}}).sort([['_id',1]])
f = open("DALBA_" + filename + ".csv", "w")
header = 'server_date;'+'id;'+'device_timestamp;'+'tips;' + \
    'Radiation;'+'temp;'+'rh;'+'signal;' + 'CRC16'+'\n'
f.write(header)
for idx, val in enumerate(cursor):
    # print(val['createdAt'].strftime("%d/%m/%Y %H:%M:%S"),';',val['raw'])
    row = str(val['createdAt'].strftime("%d/%m/%Y %H:%M:%S")) + \
        ';' + str(val['raw']).replace('.', ',') + '\n'
    var_list = row.split(";")
    row = var_list[0] + ';' + var_list[1] + var_list[2] + ';' + var_list[3] + ';' + var_list[4] + \
        ';' + var_list[5] + ';' + var_list[6] + \
        ';' + var_list[7].replace('\n', '') + '\n'

    if var_list[2] == '35':
        row = var_list[0]+';'+var_list[1]+var_list[2]+';'+var_list[3]+';' + \
            var_list[7].replace('\n', '')+';'+var_list[6] + \
            ';'+var_list[4]+';'+var_list[5]+'\n'
        print(row)

    print(row)
    f.write(row)
# filename = 'ss1_' + datetime.datetime.utcnow().strftime("%Y_%m_%d_%H") + '.csv'
# # upload file on server
# ftp = FTP('krafty-com-br.umbler.net')
# ftp.login('krafty-com-br', 'x:_Zc(O}}74=>yW')
# ftp.cwd('public/pluvion')
# with open("pluvion.csv", 'rb') as outfile:
#     ftp.storbinary('STOR ' + filename, outfile)
#     print(filename, '\n', os.path.getsize("pluvion.csv"), " bytes Successfully uploaded!")
# # ftp.retrlines('LIST')
# ftp.quit()
