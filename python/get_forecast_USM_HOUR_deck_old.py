import requests
import json
import datetime
import pytz
import os

API_TOKEN = os.environ.get(
    'API_TOKEN',
    'Specified environment variable is not set.'
)

utcmoment_naive = datetime.datetime.utcnow()
utcmoment = utcmoment_naive.replace(tzinfo=pytz.utc)
yesterday = (utcmoment - datetime.timedelta(days=1))

url = "https://api.dashboard.pluvion.com.br/reports/forecast"

# JEITO BASICO DE DATA
# filedate = "%d" % utcmoment_naive.day
# filemonth = "%d" % utcmoment_naive.month

offsets = list(range(10))
# offsets = [0]
for of in offsets:
    offsetdate = (utcmoment - datetime.timedelta(days=of))
    filedate = offsetdate.strftime("%d")
    filemonth = offsetdate.strftime("%m")

    f = open("USM_FCT_HOURLY_"+str(filemonth)+"_"+str(filedate)+".csv", "w")

    querystring = {"stations": "USM_1,USM_2,USM_3,USM_4,USM_5", "offset": of}
    headers = {
        'Authorization': API_TOKEN,
        'x-user-authorization': "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6InIxVVd2eWdhTSIsInBhcnRuZXJJRCI6InIxU2pyWTh0eiIsImN1c3RvbWVySUQiOiJIa2daNzF4NkciLCJuYW1lIjoiRnJlZGVyaWNvIFNpYW5zaSIsInR5cGUiOiJ1c2VyIiwicm9sZXMiOiJjdXN0b21lciIsImlhdCI6MTUyNDc4Njk0MiwiZXhwIjoxNTU2MzIyOTQyLCJhdWQiOiJ1cm46YXBpLnBsdXZpb24uY29tLmJyL2N1c3RvbWVyIiwiaXNzIjoicGx1dmlvbi5jb20uYnIifQ.c_UvaTH-F_1K-MkpNC9xiZ9RGb2nOOgushZTxFW82jY",
        'Cache-Control': "no-cache",
        'Postman-Token': "57bc7430-70a1-476e-b821-003c72e9fc47"
    }

    response = requests.request(
        "GET", url, headers=headers, params=querystring)
    jsonpayload = json.loads(response.text, encoding="utf-8")
    header = 'forecast_type;' + 'id;' + 'offsetdate;' + \
        'timestamp;' + 'pc;' + 'rh;' + 'tp;' + 'rd;' + '\n'
    f.write(header)
    stations = ["USM_1", "USM_2", "USM_3", "USM_4", "USM_5"]
    hours = list(range(240))
    try:
        for st in stations:
            try:
                for h in hours:
                    row = "sd10dm" + ";" + st + ";" + str(offsetdate.strftime("%Y-%m-%d")) + ";" + jsonpayload[st]["sd10dm"]["hourly"]["hour"][h] + ";" + str(
                        jsonpayload[st]["sd10dm"]["hourly"]["pc"][h]).replace(".", ",") + ";" + str(
                        jsonpayload[st]["sd10dm"]["hourly"]["rh"][h]).replace(".", ",") + ";" + str(
                        jsonpayload[st]["sd10dm"]["hourly"]["tp"][h]).replace(".", ",") + ";" + str(
                        jsonpayload[st]["sd10dm"]["hourly"]["rd"][h]).replace(".", ",") + ";" + '\n'
                    print(row)
                    f.write(row)

            except:
                error = "sd10dm" + ";" + st + ';' + \
                    str(offsetdate.strftime("%Y-%m-%d")) + \
                    ";" + "missing hour" + '\n'
                print(error)
                f.write(error)
                pass
    except:
        pass

    f.write('\n')
    f.write('\n')

    # CAPTURAR FORECASTS ORIGINAIS
    # hd5d
    stations = ["USM_1", "USM_2", "USM_3", "USM_4", "USM_5"]
    hours = list(range(120))
    try:
        for st in stations:
            try:
                for h in hours:
                    row = "hd5d" + ";" + st + ";" + str(offsetdate.strftime("%Y-%m-%d")) + ";" + \
                          jsonpayload[st]["hd5d"]["hourly"]["hour"][h] + ";" + str(
                        jsonpayload[st]["hd5d"]["hourly"]["pc"][h]).replace(".", ",") + ";" + str(
                        jsonpayload[st]["hd5d"]["hourly"]["rh"][h]).replace(".", ",") + ";" + str(
                        jsonpayload[st]["hd5d"]["hourly"]["tp"][h]).replace(".", ",") + ";" + str(
                        jsonpayload[st]["hd5d"]["hourly"]["rd"][h]).replace(".", ",") + ";" + '\n'
                    print(row)
                    f.write(row)

            except:
                error = "hd5d" + ";" + st + ';' + \
                    str(offsetdate.strftime("%Y-%m-%d")) + \
                    ";" + "missing hour" + '\n'
                print(error)
                f.write(error)
                pass
    except:

        # hd5dsm
        stations = ["USM_1", "USM_2", "USM_3", "USM_4", "USM_5"]
        hours = list(range(120))
    try:
        for st in stations:
            try:
                for h in hours:
                    row = "hd5dsm" + ";" + st + ";" + str(offsetdate.strftime("%Y-%m-%d")) + ";" + \
                          jsonpayload[st]["hd5dsm"]["hourly"]["hour"][h] + ";" + str(
                        jsonpayload[st]["hd5dsm"]["hourly"]["pc"][h]).replace(".", ",") + ";" + str(
                        jsonpayload[st]["hd5dsm"]["hourly"]["rh"][h]).replace(".", ",") + ";" + str(
                        jsonpayload[st]["hd5dsm"]["hourly"]["tp"][h]).replace(".", ",") + ";" + str(
                        jsonpayload[st]["hd5dsm"]["hourly"]["rd"][h]).replace(".", ",") + ";" + '\n'
                    print(row)
                    f.write(row)

            except:
                error = "hd5dsm" + ";" + st + ';' + \
                    str(offsetdate.strftime("%Y-%m-%d")) + \
                    ";" + "missing hour" + '\n'
                print(error)
                f.write(error)
                pass
    except:

        # sd10d
        stations = ["USM_1", "USM_2", "USM_3", "USM_4", "USM_5"]
        hours = list(range(240))
    try:
        for st in stations:
            try:
                for h in hours:
                    row = "sd10d" + ";" + st + ";" + str(offsetdate.strftime("%Y-%m-%d")) + ";" + \
                          jsonpayload[st]["sd10d"]["hourly"]["hour"][h] + ";" + str(
                        jsonpayload[st]["sd10d"]["hourly"]["pc"][h]).replace(".", ",") + ";" + str(
                        jsonpayload[st]["sd10d"]["hourly"]["rh"][h]).replace(".", ",") + ";" + str(
                        jsonpayload[st]["sd10d"]["hourly"]["tp"][h]).replace(".", ",") + ";" + str(
                        jsonpayload[st]["sd10d"]["hourly"]["rd"][h]).replace(".", ",") + ";" + '\n'
                    print(row)
                    f.write(row)

            except:
                error = "sd10d" + ";" + st + ';' + \
                    str(offsetdate.strftime("%Y-%m-%d")) + \
                    ";" + "missing hour" + '\n'
                print(error)
                f.write(error)
                pass
    except:
        continue

    f.close()
