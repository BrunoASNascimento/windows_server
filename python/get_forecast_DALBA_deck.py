import requests
import json
import datetime
import pytz
import os

API_TOKEN = os.environ.get(
    'API_TOKEN',
    'Specified environment variable is not set.'
)

utcmoment_naive = datetime.datetime.utcnow()
utcmoment = utcmoment_naive.replace(tzinfo=pytz.utc)
yesterday = (utcmoment - datetime.timedelta(days=1))

url = "https://api.dashboard.pluvion.com.br/reports/forecast"

# JEITO BASICO DE DATA
# filedate = "%d" % utcmoment_naive.day
# filemonth = "%d" % utcmoment_naive.month

offsets = list(range(2))
# offsets = [0]
for of in offsets:
    offsetdate = (utcmoment - datetime.timedelta(days=of))
    filedate = offsetdate.strftime("%d")
    filemonth = offsetdate.strftime("%m")

    f = open("DALBA_FCT_"+str(filemonth)+"_"+str(filedate)+".csv", "w")

    querystring = {"stations": "DALBA_41BF05,DALBA_41C523", "offset": of}
    headers = {
        'Authorization': API_TOKEN,
        'x-user-authorization': API_TOKEN
    }
    #print("query: ",querystring)

    response = requests.request(
        "GET", url, headers=headers, params=querystring)
    #print("api responde: ",response.text)
    jsonpayload = json.loads(response.text, encoding="utf-8")
    header = 'forecast_type;'+'id;'+'offsetdate;'+'timestamp;'+'pc' + '\n'
    f.write(header)
    stations = ["DALBA_41BF05", "DALBA_41C523"]
    days = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    try:
        for st in stations:
            try:
                for d in days:
                    row = "sd10d" + ";" + st + ";" + str(offsetdate.strftime("%Y-%m-%d")) + ";" + jsonpayload[st]["sd10dm"]["daily"]["day"][d] + ";" + str(
                        jsonpayload[st]["sd10d"]["daily"]["pcAcc"][d]).replace(".", ",") + '\n'
                    print(row)
                    f.write(row)

            except:
                error = "sd10d" + ";" + st + ';' + \
                    str(offsetdate.strftime("%Y-%m-%d")) + \
                    ";" + "missing day" + '\n'
                print(error)
                f.write(error)
                pass
    except:

        stations = ["DALBA_41BC05", "DALBA_41C523"]
    try:
        for st in stations:
            days = jsonpayload[st]["ld30d"]["daily"]["day"]
            for idx, val in enumerate(days[10:]):
                row = "ld30d" + ";" + st + ";" + str(offsetdate.strftime("%Y-%m-%d")) + ";" + val + ";" + str(
                    jsonpayload[st]["ld30d"]["daily"]["pcAcc"][idx + 10]).replace(".", ",") + '\n'
                print(row)
                f.write(row)
    except:
        f.write('Error getting ld30d at ' + st +
                str(offsetdate.strftime("%Y-%m-%d")) + ";" + '\n')

    # f.write('\n')
    # f.write('\n')

    # CAPTURAR FORECASTS ORIGINAIS
    # hd5d
    # stations = ["DALBA_1", "DALBA_2", "DALBA_3"]
    # days = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    # try:
    #     for st in stations:
    #         try:
    #             for d in days:
    #                 row = "hd5d" + ";" + st + ";" + str(offsetdate.strftime("%Y-%m-%d")) + ";" + jsonpayload[st]["hd5d"]["daily"]["day"][d] + ";" + str(
    #                     jsonpayload[st]["hd5d"]["daily"]["pcAcc"][d]).replace(".", ",") + '\n'
    #                 print(row)
    #                 f.write(row)
    #
    #
    #         except:
    #             error = "hd5d" + ";" + st + ';' + str(offsetdate.strftime("%Y-%m-%d")) + ";" + "missing day" + '\n'
    #             print(error)
    #             f.write(error)
    #             pass
    # except:
    #
    # # hd5dsm
    #     stations = ["DALBA_1", "DALBA_2", "DALBA_3"]
    #     days = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    # try:
    #     for st in stations:
    #         try:
    #             for d in days:
    #                 row = "hd5dsm" + ";" + st + ";" + str(offsetdate.strftime("%Y-%m-%d")) + ";" + jsonpayload[st]["hd5dsm"]["daily"]["day"][d] + ";" + str(
    #                     jsonpayload[st]["hd5dsm"]["daily"]["pcAcc"][d]).replace(".", ",") + '\n'
    #                 print(row)
    #                 f.write(row)
    #
    #
    #         except:
    #             error = "hd5dsm" + ";" + st + ';' + str(offsetdate.strftime("%Y-%m-%d")) + ";" + "missing day" + '\n'
    #             print(error)
    #             f.write(error)
    #             pass
    # except:
    #
    # # sd10d
    #     stations = ["DALBA_1", "DALBA_2", "DALBA_3"]
    #     days = [0,1,2,3,4,5,6,7,8,9]
    # try:
    #     for st in  stations:
    #         try:
    #             for d in days:
    #                 row = "sd10d" + ";" + st + ";" + str(offsetdate.strftime("%Y-%m-%d")) + ";" + jsonpayload[st]["sd10d"]["daily"]["day"][d] + ";" + str(
    #                     jsonpayload[st]["sd10d"]["daily"]["pcAcc"][d]).replace(".", ",") + '\n'
    #                 print(row)
    #                 f.write(row)
    #
    #
    #         except:
    #             error = "sd10d" + ";" + st + ';' + str(offsetdate.strftime("%Y-%m-%d")) + ";" + "missing day" + '\n'
    #             print(error)
    #             f.write(error)
    #             pass
    # except:
    #     continue

    f.close()
