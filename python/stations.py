import os
import requests
import json

MONGO_URL = os.environ.get(
    'MONGO_URL',
    'Specified environment variable is not set.'
)


def get_stations_mongo():
    from pymongo import MongoClient
    import ssl
    client = MongoClient(
        MONGO_URL,
        ssl_cert_reqs=ssl.CERT_NONE)
    db = client['api-wtr']
    cursor = db.stations.find({}).sort([['stationID', 1]])
    stations = []
    for idx, val in enumerate(cursor):
        stations.append(
            [val['forecastTypes'], val['stationHWID'], val['name'], val['coordinates']])
    print(stations)
    return(stations)


def get_stations_api():
    import requests
    import json
    url = 'https://us-central1-pluvion-tech.cloudfunctions.net/stations/select'
    response = requests.request(
        "GET", url)
    jsonpayload = json.loads(response.text, encoding="utf-8")
    print(jsonpayload['data'][0])


get_stations_api()
