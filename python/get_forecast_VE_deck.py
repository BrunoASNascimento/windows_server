import requests
import json
import datetime
import pytz
import os

API_TOKEN = os.environ.get(
    'API_TOKEN',
    'Specified environment variable is not set.'
)

utcmoment_naive = datetime.datetime.utcnow()
utcmoment = utcmoment_naive.replace(tzinfo=pytz.utc)
yesterday = (utcmoment - datetime.timedelta(days=1))

url = "https://api.dashboard.pluvion.com.br/reports/forecast"
token = API_TOKEN

# JEITO BASICO DE DATA
# filedate = "%d" % utcmoment_naive.day
# filemonth = "%d" % utcmoment_naive.month

offsets = list(range(2))
# offsets = [0]
for of in offsets:
    offsetdate = (utcmoment - datetime.timedelta(days=of))
    filedate = offsetdate.strftime("%d")
    filemonth = offsetdate.strftime("%m")
    fileyear = offsetdate.strftime("%Y")

    f = open("VE_FCT_"+str(fileyear)+str(filemonth)+str(filedate)+".csv", "w")

    querystring = {
        "stations": "VTEN_1,VTEN_2,VTEN_3,VTEN_4,VTEN_5,VTEN_6", "offset": of}
    headers = {
        'Authorization': token,
        'x-user-authorization': token,
        'Cache-Control': "no-cache",
        'Postman-Token': "57bc7430-70a1-476e-b821-003c72e9fc47"
    }

    response = requests.request(
        "GET", url, headers=headers, params=querystring)
    jsonpayload = json.loads(response.text, encoding="utf-8")
    header = 'forecast_type;'+'id;'+'offsetdate;'+'timestamp;'+'pc' + '\n'
    f.write(header)
    stations = ["VTEN_1", "VTEN_2", "VTEN_3", "VTEN_4", "VTEN_5", "VTEN_6"]
    days = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    try:
        for st in stations:
            try:
                for d in days:
                    row = "sd10m" + ";" + st + ";" + str(offsetdate.strftime("%Y-%m-%d")) + ";" + jsonpayload[st]["sd10dm"]["daily"]["day"][d] + ";" + str(
                        jsonpayload[st]["sd10dm"]["daily"]["pcAcc"][d]).replace(".", ",") + '\n'
                    print(row)
                    f.write(row)

            except:
                error = "sd10m" + ";" + st + ';' + \
                    str(offsetdate.strftime("%Y-%m-%d")) + \
                    ";" + "missing day" + '\n'
                print(error)
                f.write(error)
                pass
    except:
        stations = ["VTEN_1", "VTEN_2", "VTEN_3", "VTEN_4", "VTEN_5", "VTEN_6"]

    try:
        for st in stations:
            days = jsonpayload[st]["ld30d"]["daily"]["day"]
            for idx, val in enumerate(days[11:]):
                row = "ld30d" + ";" + st + ";" + str(offsetdate.strftime("%Y-%m-%d")) + ";" + val + ";" + str(
                    jsonpayload[st]["ld30d"]["daily"]["pcAcc"][idx + 11]).replace(".", ",") + '\n'
                print(row)
                f.write(row)

    except:
        f.write('Error getting ld30d at ' + st +
                str(offsetdate.strftime("%Y-%m-%d")) + ";" + '\n')

    f.write('\n')
    f.write('\n')

    # CAPTURAR FORECASTS ORIGINAIS
    # hd5d
    stations = ["VTEN_1", "VTEN_2", "VTEN_3", "VTEN_4", "VTEN_5", "VTEN_6"]
    days = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    try:
        for st in stations:
            try:
                for d in days:
                    row = "hd5d" + ";" + st + ";" + str(offsetdate.strftime("%Y-%m-%d")) + ";" + jsonpayload[st]["hd5d"]["daily"]["day"][d] + ";" + str(
                        jsonpayload[st]["hd5d"]["daily"]["pcAcc"][d]).replace(".", ",") + '\n'
                    print(row)
                    f.write(row)

            except:
                error = "hd5d" + ";" + st + ';' + \
                    str(offsetdate.strftime("%Y-%m-%d")) + \
                    ";" + "missing day" + '\n'
                print(error)
                f.write(error)
                pass

    except:
        # hd5dsm
        stations = ["VTEN_1", "VTEN_2", "VTEN_3", "VTEN_4", "VTEN_5", "VTEN_6"]
        days = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

    try:
        for st in stations:
            try:
                for d in days:
                    row = "hd5dsm" + ";" + st + ";" + str(offsetdate.strftime("%Y-%m-%d")) + ";" + jsonpayload[st]["hd5dsm"]["daily"]["day"][d] + ";" + str(
                        jsonpayload[st]["hd5dsm"]["daily"]["pcAcc"][d]).replace(".", ",") + '\n'
                    print(row)
                    f.write(row)

            except:
                error = "hd5dsm" + ";" + st + ';' + \
                    str(offsetdate.strftime("%Y-%m-%d")) + \
                    ";" + "missing day" + '\n'
                print(error)
                f.write(error)
                pass

    except:
        # sd10d
        stations = ["VTEN_1", "VTEN_2", "VTEN_3", "VTEN_4", "VTEN_5", "VTEN_6"]
        days = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

    try:
        for st in stations:
            try:
                for d in days:
                    row = "sd10d" + ";" + st + ";" + str(offsetdate.strftime("%Y-%m-%d")) + ";" + jsonpayload[st]["sd10d"]["daily"]["day"][d] + ";" + str(
                        jsonpayload[st]["sd10d"]["daily"]["pcAcc"][d]).replace(".", ",") + '\n'
                    print(row)
                    f.write(row)

            except:
                error = "sd10d" + ";" + st + ';' + \
                    str(offsetdate.strftime("%Y-%m-%d")) + \
                    ";" + "missing day" + '\n'
                print(error)
                f.write(error)
                pass

    except:
        continue

    f.close()
