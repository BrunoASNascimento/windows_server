from pymongo import MongoClient
import ssl
from pymongo import MongoClient
import ssl
import time
import datetime
import pytz
from ftplib import FTP
import os

MONGO_URL = os.environ.get(
    'MONGO_URL',
    'Specified environment variable is not set.'
)


def get_stations():
    client = MongoClient(
        MONGO_URL,
        ssl_cert_reqs=ssl.CERT_NONE)
    db = client['api-wtr']
    cursor = db.stations.find({}).sort([['stationID', 1]])
    stationHWID = []
    name = []
    for idx, val in enumerate(cursor):
        stationHWID.append(val['stationHWID'])
        name.append(val['name'])
    #print(stationHWID, name)
    return stationHWID, name


def get_obs(stationHWID, name):
    print(stationHWID, name)

    num_days = 2  # Alterar esse numero para pegar historico maiores
    utcmoment_naive = datetime.datetime.utcnow()
    utcmoment = utcmoment_naive.replace(tzinfo=pytz.utc)
    set_date = (utcmoment - datetime.timedelta(days=num_days))
    name_file = utcmoment_naive.strftime("%Y%m%d")

    # print(utcmoment_naive)
    # print(utcmoment)
    # print(name_file)
    print('Number of days: ', num_days, '\n')

    # convert your date string to datetime object
    start = set_date  # datetime.datetime(2018, 4, 1, 0, 0, 00)
    end = utcmoment
    # print(start)
    # print(end)

    client = MongoClient(
        MONGO_URL, ssl_cert_reqs=ssl.CERT_NONE)
    db = client['api-wtr']
    cursor = db.weather.find({'createdAt': {'$lt': end, '$gte': start}, 'stationHWID': {
                             '$in': stationHWID}}).sort([['_id', 1]])

    f = open("teste_"+name_file+".csv", "w")

    header = 'server_date;' + 'type;' + 'id;' + \
        'device_timestamp;' + 'temp;'+'rh;'+'ws;'+'wd;'+'bt;rain;'+'\n'  # tamoios

    f.write(header)

    for idx, val in enumerate(cursor):

        var_list = str(val['raw']).replace('.', ',').split(";")
        row = ''
        row = str(val['createdAt'].strftime("%d/%m/%Y %H:%M:%S")) + ';' + str(
            val['raw']).replace('.', ',') + ';' + str(val['pc']).replace('.', ',') + '\n'
        print(row)
        f.write(row)


name, stationHWID = get_stations()
get_obs(name, stationHWID)
