import pluvion
from datetime import datetime
import time

#What time is it?
utcmoment_naive = datetime.utcnow()
tm = utcmoment_naive
tm = int(time.mktime(tm.timetuple()))

dev_string = 'bcce5f04466c'
list_payload =\
    [
'1549044840288011000000000000000000',
'15490449960A95AA804FD34FE64FDB213A',
'1549044996188C0B0400276569652B6435',
'15490469910E4000005460CF8000000000',
'1549046991186E0180002BC5786578614A',
'15490486040A754A404FC04FED4FDC2B8A',
]
# generate csv files and its headers
if (dev_string =='bcce5f04466c'):
	nome = 'pedreira'
else:
	nome = 'PCH'
	
f_temp = open(nome +"_"+str(tm)+"_"+dev_string+"_sigfox_temp.csv", "w")
f_temp.write('device' + ';'+ 'type' + ';'+ 'data' + ';' + 'Minimum humidity' + ';' + 'Maximum humidity' + ';' + 'Average humidity' + ';' + 'Minimum temperature' + ';' + 'Maximum temperature' + ';' + 'Average temperature' + ';' + 'Variance temperature' + '\n')

f_rain = open(nome +"_"+str(tm)+"_"+dev_string+"_sigfox_rain.csv", "w")
f_rain.write('device' + ';'+'timestamp'+';' + 'data' + ';' + 'payload' + ';' + 'tips' + '\n')

f_wind = open(nome +"_"+str(tm)+"_"+dev_string+"_sigfox_wind.csv", "w")
f_wind.write('device'+ ';'+ 'type'+ ';' + 'data' + ';' + 'Maximum Wind speed' + ';' + 'Wind direction in max speed' + ';' + 'Wind speed vectorial average' + ';' + 'Wind dir for vectorial erage' + ';' + 'Wind speed last 5 min' + ';' + 'Minimum solar irradiance' + ';' + 'Maximum solar irradiance' + ';' + 'Average solar irradiance' + ';' + 'Variance solar irradiance' + '\n')
f_mgmt = open(nome +"_"+str(tm)+"_"+dev_string+"_sigfox_mgmt.csv", "w")
f_mgmt.write('device' + ';' + 'data' + ';' + 'Rain sensor error' + ';' + 'Temperature sensor error' + ';' + 'Humidity sensor error' + ';' + 'Wind speed sensor error' + ';' + 'Wind direction sensor error' + ';' + 'Irradiation sensor error' + ';' + 'Electric failure sensor error' + ';' + 'Battery percentage' + ';' + 'Use of battery in last 24 hrs' + ';' + 'Firmware version' + ';' + 'External voltage' + '\n')



for idx,val in enumerate(list_payload):
    timestamp = int(val[:10])
    timestamp = datetime.utcfromtimestamp(timestamp).strftime("%Y-%m-%d %H:%M:%S")
    payload =val[10:]
    parsed_payload = pluvion.do_payload_parser(payload,timestamp)
    msg_id = parsed_payload[0]
    if msg_id == 0:
        f_mgmt.write(dev_string+';'+pluvion.convert_list2csv(parsed_payload[1])+'\n')
    elif msg_id == 1 or msg_id == 2:
        f_temp.write(dev_string + ';' + pluvion.convert_list2csv(parsed_payload[1]) + '\n')
    elif msg_id == 3 or msg_id == 4:
        f_wind.write(dev_string + ';' + pluvion.convert_list2csv(parsed_payload[1]) + '\n')
    elif msg_id == 5:
        for r in parsed_payload[1]:
            f_rain.write(dev_string+';'+str(r[0])+';'+str(r[1])+';'+payload+';'+str(r[2])+'\n')