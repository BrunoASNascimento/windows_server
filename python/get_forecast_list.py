from pymongo import MongoClient
import ssl
import os 

MONGO_URL = os.environ.get(
    'MONGO_URL',
    'Specified environment variable is not set.'
)

def get_stations():
    client = MongoClient(
        MONGO_URL,
        ssl_cert_reqs=ssl.CERT_NONE)
    db = client['api-wtr']
    cursor = db.stations.find({}).sort([['stationID', 1]])
    stations = []
    for idx, val in enumerate(cursor):
        stations.append([val['forecastTypes'], val['stationHWID'],
                         val['name'], val['coordinates'], val['stationID']])
    print(stations)
    return(stations)


stations = get_stations()


def get_forecast(fcst_type, coord_list):
    client = MongoClient(
        MONGO_URL,
        ssl_cert_reqs=ssl.CERT_NONE)
    db = client['api-wtr']
    fcstdb = {'HD5D': 'HD5DForecast',
              'SD10D': 'SD10DForecast', 'SD10DM': 'SD10DMForecast'}
    print(coord_list)
    if fcst_type == 'HD5D':
        cursor = db.HD5DForecast.find({'coordinates': {'$in': coord_list}}).sort([
            ['createdAt', -1]]).limit(len(coord_list))
    elif fcst_type == 'SD10D':
        cursor = db.SD10DForecast.find({'coordinates': {'$in': coord_list}}).sort([
            ['createdAt', -1]]).limit(len(coord_list))
    elif fcst_type == 'SD10DM':
        cursor = db.SD10DMForecast.find({'coordinates': {'$in': coord_list}}).sort([
            ['createdAt', -1]]).limit(len(coord_list))
    # fcst_response={}
    for idx, val in enumerate(cursor):
        print(idx, val['forecastDate'], val['coordinates'])
        # fcst_response[val['coordinates']]=val['forecastDate']
    return


coord_list = []
for st in stations:
    coord_list.append(st[4])
# print(coord_list)
get_forecast('HD5D', coord_list)
