import requests
import json
import datetime
import pytz
import os

API_TOKEN = os.environ.get(
    'API_TOKEN',
    'Specified environment variable is not set.'
)

utcmoment_naive = datetime.datetime.utcnow()
utcmoment = utcmoment_naive.replace(tzinfo=pytz.utc)
yesterday = (utcmoment - datetime.timedelta(days=1))

stations = ["001D0A00D0D9", "001D0A00D5AB", "001D0A00D684", "001D0A00E2C7", "001D0A00E305", "LORA_0059B6A7C3EE8629",
            "PLUVION_41C500", "PLUVION_41C593", "PLUVION_41C786"]

url = "https://api.dashboard.pluvion.com.br/reports/forecast"
token = API_TOKEN

# JEITO BASICO DE DATA
# filedate = "%d" % utcmoment_naive.day
# filemonth = "%d" % utcmoment_naive.month

list_stations = ""
for st in stations:
    list_stations = list_stations + st + ','

offsets = list(range(10))
#offsets = [0]
for of in offsets:
    offsetdate = (utcmoment - datetime.timedelta(days=of))
    filedate = offsetdate.strftime("%d")
    filemonth = offsetdate.strftime("%m")
    fileyear = offsetdate.strftime("%Y")

    f = open("OUTHERS_STATIONS_FCT_"+str(fileyear) +
             str(filemonth)+str(filedate)+".csv", "w")

    querystring = {"stations": list_stations, "offset": of}
    headers = {
        'Authorization': token,
        'x-user-authorization': token,
        'Cache-Control': "no-cache",
        'Postman-Token': "57bc7430-70a1-476e-b821-003c72e9fc47"
    }

    response = requests.request(
        "GET", url, headers=headers, params=querystring)
    jsonpayload = json.loads(response.text, encoding="utf-8")
    header = 'forecast_type;' + 'id;' + 'offsetdate;' + 'timestamp;' + 'pc' + '\n'

    f.write(header)

    hours = list(range(240))
    try:
        for st in stations:
            try:
                for h in hours:
                    row = "sd10dm" + ";" + st + ";" + str(offsetdate.strftime("%Y-%m-%d")) + ";" + jsonpayload[st]["sd10dm"]["hourly"]["hour"][h] + ";" + str(
                        jsonpayload[st]["sd10dm"]["hourly"]["pc"][h]) + '\n'
                    print(row)
                    f.write(row)
            except:
                error = "sd10dm" + ";" + st + ';' + \
                    str(offsetdate.strftime("%Y-%m-%d")) + \
                    ";" + "missing hour" + '\n'
                print(error)
                f.write(error)
                pass
    except:
        pass

    f.write('\n')
    f.write('\n')

    # CAPTURAR FORECASTS ORIGINAIS
    # hd5d
    hours = list(range(120))
    try:
        for st in stations:
            try:
                for h in hours:
                    row = "hd5d" + ";" + st + ";" + str(offsetdate.strftime("%Y-%m-%d")) + ";" + \
                          jsonpayload[st]["hd5d"]["hourly"]["hour"][h] + ";" + str(
                        jsonpayload[st]["hd5d"]["hourly"]["pc"][h]) + '\n'
                    print(row)
                    f.write(row)
            except:
                error = "hd5d" + ";" + st + ';' + \
                    str(offsetdate.strftime("%Y-%m-%d")) + \
                    ";" + "missing hour" + '\n'
                print(error)
                f.write(error)
                pass
    except:
        # hd5dsm
        hours = list(range(120))
    try:
        for st in stations:
            try:
                for h in hours:
                    row = "hd5dsm" + ";" + st + ";" + str(offsetdate.strftime("%Y-%m-%d")) + ";" + \
                          jsonpayload[st]["hd5dsm"]["hourly"]["hour"][h] + ";" + str(
                        jsonpayload[st]["hd5dsm"]["hourly"]["pc"][h]) + '\n'
                    print(row)
                    f.write(row)
            except:
                error = "hd5dsm" + ";" + st + ';' + \
                    str(offsetdate.strftime("%Y-%m-%d")) + \
                    ";" + "missing hour" + '\n'
                print(error)
                f.write(error)
                pass
    except:
        # sd10d
        hours = list(range(240))
    try:
        for st in stations:
            try:
                for h in hours:
                    row = "sd10d" + ";" + st + ";" + str(offsetdate.strftime("%Y-%m-%d")) + ";" + \
                          jsonpayload[st]["sd10d"]["hourly"]["hour"][h] + ";" + str(
                        jsonpayload[st]["sd10d"]["hourly"]["pc"][h]) + '\n'
                    print(row)
                    f.write(row)
            except:
                error = "sd10d" + ";" + st + ';' + \
                    str(offsetdate.strftime("%Y-%m-%d")) + \
                    ";" + "missing hour" + '\n'
                print(error)
                f.write(error)
                pass
    except:
        continue

    f.close()
