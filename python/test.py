from pymongo import MongoClient
import ssl
import json
import pandas as pd
import os

MONGO_URL = os.environ.get(
    'MONGO_URL',
    'Specified environment variable is not set.'
)


def get_stations():
    client = MongoClient(
        MONGO_URL,
        ssl_cert_reqs=ssl.CERT_NONE)
    db = client['api-wtr']
    cursor = db.stations.find({}).sort([['stationID', 1]])
    # print(cursor)
    stations = []
    df = pd.DataFrame()
    for idx, val in enumerate(cursor):
        stations.append(
            [val['forecastTypes'], val['stationHWID'], val['name'], val['coordinates']])
        df = df.append(val, ignore_index=True)
        print(val)
    print(df)
    #jsonpayload = json.loads(cursor, encoding="utf-8")
    # print(jsonpayload)
    return(stations)


stations = get_stations()


def get_forecast(stations):
    client = MongoClient(
        MONGO_URL,
        ssl_cert_reqs=ssl.CERT_NONE)
    db = client['api-wtr']
    # print(coord_list)
    if fcst_type == 'HD5D':
        cursor = db.HD5DForecast.find({'coordinates': {'$in': coord_list}}).sort([
            ['createdAt', -1]]).limit(len(coord_list))
    elif fcst_type == 'SD10D':
        cursor = db.SD10DForecast.find({'coordinates': {'$in': coord_list}}).sort([
            ['createdAt', -1]]).limit(len(coord_list))
    elif fcst_type == 'SD10DM':
        cursor = db.SD10DMForecast.find({'coordinates': {'$in': coord_list}}).sort([
            ['createdAt', -1]]).limit(len(coord_list))

    # for idx, val in enumerate(cursor):

    return (fcst_response)
