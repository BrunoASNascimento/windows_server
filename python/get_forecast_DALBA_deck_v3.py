import requests
import json
import datetime
import pytz
import os

API_TOKEN = os.environ.get(
    'API_TOKEN',
    'Specified environment variable is not set.'
)
utcmoment_naive = datetime.datetime.utcnow()
utcmoment = utcmoment_naive.replace(tzinfo=pytz.utc)
yesterday = (utcmoment - datetime.timedelta(days=1))

url = "https://api.dashboard.pluvion.com.br/reports/forecast"
token = API_TOKEN


# JEITO BASICO DE DATA
# filedate = "%d" % utcmoment_naive.day
# filemonth = "%d" % utcmoment_naive.month

# PARAMETROS
offsets = list(range(10))
#offsets = [0]
stations = ["DALBA_41BF05", "DALBA_41C523"]
days = range(10)

print("offsets:", offsets)
for of in offsets:
    offsetdate = (utcmoment - datetime.timedelta(days=of))
    filedate = offsetdate.strftime("%d")
    filemonth = offsetdate.strftime("%m")
    fileyear = offsetdate.strftime("%Y")

    # Cria CSV
    f = open("DALBA_FCT_"+str(fileyear) +
             str(filemonth)+str(filedate)+".csv", "w")

    list_stations = ""
    for st in stations:
        list_stations = list_stations + st + ','

    querystring = {"stations": list_stations, "offset": of}
    headers = {
        'Authorization': token,
        'x-user-authorization': token
    }
    response = requests.request(
        "GET", url, headers=headers, params=querystring)
    jsonpayload = json.loads(response.text, encoding="utf-8")
    header = 'forecast_type;'+'id;'+'offsetdate;'+'timestamp;'+'pc' + '\n'
    f.write(header)
    for st in stations:

        for d in days:
            try:
                # Edita o nome para o antigo nome
                if (st == "DALBA_41BF05"):
                    st_name = "DALBA_2"
                else:
                    st_name = "DALBA_1"

                row = "sd10d" + ";" + st_name + ";" + str(offsetdate.strftime("%Y-%m-%d")) + ";" + jsonpayload[st]["sd10d"]["daily"]["day"][d] + ";" + str(
                    jsonpayload[st]["sd10d"]["daily"]["pcAcc"][d]).replace(".", ",") + '\n'
                print(row)
                print(jsonpayload)
                f.write(row)
            except:
                error = "sd10d" + ";" + st + ';' + \
                    str(offsetdate.strftime("%Y-%m-%d")) + \
                    ";" + "missing day" + '\n'
                print(error)
                f.write(error)
                pass
    f.close()
